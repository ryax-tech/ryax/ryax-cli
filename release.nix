{
  ryaxpkgs ? import <ryaxpkgs> { },
  appDir ? ./.
}:
rec {
  shell = with ryaxpkgs; pkgs.mkShell {
      buildInputs = [ python pkgs.poetry ];
    };

  image = ryaxpkgs.callPackage ./nix/docker.nix {
    inherit appDir;
  };
}
