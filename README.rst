========
Ryax CLI
========

This repository is part of `Ryax`_.

Ryax CLI, also referenced as CLI throughout this document, is a simple command
line interface tool to handle Ryax API calls.
To understand Ryax, please visit the `ryax docummentaion`_ first.


.. _Ryax: https://github.com/RyaxTech/ryax

.. _ryax docummentaion: https://docs.ryax.tech/


Installation
------------

.. code-block:: sh

   poetry shell
   poetry install



Usage
-----

Run Ryax CLI.

.. code-block:: sh

   ryax-cli

To get help with options run ``ryax-cli --help``.

To run a command on a specific instance, set the ``server`` variable, for example

.. code-block:: sh

   ryax-cli --server=https://myinstance.ryax.io

The first time you try to connect to an instance, your user and password will be asked.

