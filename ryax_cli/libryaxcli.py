#!/usr/bin/env python3
# Copyright (C) Ryax Technologies
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import logging
import os
import sys
from typing import Union

from .execution import Executions
from .librutils import BaseCLI, RyaxdConnexion, get_configuration
from .module import Module
from .project import Project
from .repository import Repository
from .user import User
from .workflow import Workflow


class Cli(BaseCLI):
    """
    Command line interface for Ryax API call.

    Usage:
        ryax-cli --server=https://myinstance.ryax.io login
        ryax-cli --server=https://myinstance.ryax.io workflow list

    Options:
    --server=SERVER
        Where SERVER is the complete server URL, it must be the exact Ryax
        API endpoint, for instance, `https://myinstance.ryax.io`.
        By default the port is guessed based on protocol `http` or `https`.
        If you need a specific port type it in URL notation, for instance to
        use port 9292 instead of default, use `https://app.ryax.io:9292.
        You can use the RYAX_URL environment variable to set this value. This
        option take precedance if both are set.

    -o, --ouptput_format=OUTPUT_FORMAT
        OUTPUT_FORMAT can be either human (default), json, or yaml.
        human: output lists as simple terminal formatted tables;
        json: detailed output in json format;
        yaml: detailed output in yaml format.
        For more detailed output use either `yaml` or `json`. OUTPUT_FORMAT are
        case insensitive, so either `json`, `Json`, or `JSON` will result in
        json formated output. Format human for get command retrieve the same
        as json.

    --verify_ssl=VERIFY_SSL
        VERIFY_SSL is either True (default) enabled or False disabled ssl
        authentication. Default is True, disable if you are using a mockup
        version with unsigned SSL certificates.

    --username=USERNAME
    --password=PASSWORD
        MUST be used together, USERNAME and PASSWORD avoid having to call
        login command to initiate a session.

    """

    def __init__(
        self,
        server: str = os.environ.get("RYAX_URL", "http://127.0.0.1:9292"),
        output_format: str = "HUMAN",
        verify_ssl: bool = True,
        username: str = "",
        password: str = "",
        debug: bool = False,
    ) -> None:
        if debug:
            logging.basicConfig(level=logging.DEBUG)

        self._conn = RyaxdConnexion(server, srv_verify=verify_ssl)
        super().__init__(output_format)

        # Only accept if both username and password are set
        if len(username) != 0 and len(password) == 0:
            print("Error flags --username and --password should be used together.")
            sys.exit(1)
        if len(username) == 0 and len(password) != 0:
            print("Error flags --username and --password should be used together.")
            sys.exit(1)

        if len(username) != 0 and len(password) != 0:
            self.login(username, password)

        configuration = get_configuration(self._conn, debug=debug)

        self.user = User(configuration, output_format)
        self.project = Project(configuration, output_format)
        self.pj = self.project  # alias

        self.repository = Repository(configuration, output_format)
        self.repo = self.repository  # alias

        self.workflow = Workflow(configuration, output_format)
        self.wf = self.workflow  # alias

        self.module = Module(configuration, output_format)
        self.mod = self.module  # alias

        self._exe = Executions(configuration, output_format)

    def login(self, username: str = None, password: str = None) -> Union[str, None]:
        """
        Open an API session, prompt for username and password, to avoid this use --username and --password, see --help.
        """
        res = self._conn.login(username, password)
        if self._output_format_is_human():
            print("Login successful!")
            return None
        else:
            return self._print_formatted(res)

    def whoami(self) -> Union[str, None]:
        """
        Retrieve information for the user associated with the current session.
        """
        res = self._conn.get("/authorization/me", should_be_logged=False)
        if self._output_format_is_human():
            print(f"You are logged as {res['username']}. Role: {res['role']}")
            return None
        else:
            return self._print_formatted(res)

    def logout(self) -> Union[str, None]:
        """
        Close API session.
        """
        res = self._conn.logout()
        if self._output_format_is_human():
            print(f"{res['status']}: {res['detail']}")
            return None
        else:
            return self._print_formatted(res)

    def version(self) -> str:
        """
        Show current version and exits.
        """
        import ryax_cli.version

        return f"ryax-cli: {ryax_cli.version.__version__}\npython: {ryax_cli.version.python_version}"

    def _get(self, url: str) -> str:
        headers, res = self._conn.get_with_headers(url)
        print("headers:", headers)
        return self._print_formatted(res)

    def _del(self, url: str) -> str:
        res = self._conn.delete(url)
        return self._print_formatted(res)
