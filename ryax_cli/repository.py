#!/usr/bin/env python3
# Copyright (C) Ryax Technologies
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

from typing import List, Optional, Union

import ryax_sdk
from ryax_sdk.api.repositories_api import (
    RepositoriesApi,
    RepositoryActionBuild,
    RepositoryRepositoryScan,
)
from ryax_sdk.api.repository_actions_api import RepositoryActionsApi
from ryax_sdk.model.repository_add_source import RepositoryAddSource
from ryax_sdk.model.repository_source import RepositorySource
from ryax_sdk.model.repository_source_scan_request import RepositorySourceScanRequest
from ryax_sdk.model.repository_update_source import RepositoryUpdateSource

from .librutils import BaseCLIWithConfig, print_error, print_list_of_dict_as_table


class RepositoryModules(BaseCLIWithConfig):
    """
    Handle Repository Modules operations.
    """

    def __init__(
        self, configuration: ryax_sdk.Configuration, output_format: str
    ) -> None:
        super().__init__(configuration, output_format)
        self._api_client = ryax_sdk.ApiClient(configuration)

    def list(self) -> Optional[str]:
        """
        List repository modules.
        """
        api_instance = RepositoryActionsApi(self._api_client)
        res: List[RepositoryActionBuild] = api_instance.repository_modules_get

        res_dict = []
        for result in res:
            result_in_dict = result.to_dict()
            result_in_dict["source"] = result_in_dict["source"]["name"]
            res_dict.append(result_in_dict)

        if self._output_format_is_human():
            print_list_of_dict_as_table(
                res_dict, ["id", "name", "source", "kind", "status"], sort_by="source"
            )
            return None
        return self._print_formatted(res_dict)

    def build(self, module_id: str) -> Optional[str]:
        """
        Build a module
        """
        api_instance = RepositoryActionsApi(self._api_client)
        res: RepositoryActionBuild = (
            api_instance.repository_modules_module_id_build_post(module_id)
        )
        if self._output_format_is_human():
            print(f"Repository module {res.id} new status: {res.status}")
            return None
        return self._print_formatted(res.to_dict())

    def delete(self, module_id: str) -> None:
        """
        Delete a repository module
        """
        api_instance = RepositoryActionsApi(self._api_client)
        api_instance.repository_modules_module_id_delete(module_id)

        if self._output_format_is_human():
            print(f"Repository module {module_id} deleted")


class Repository(BaseCLIWithConfig):
    """
    Handle Repository operations.
    """

    def __init__(
        self, configuration: ryax_sdk.Configuration, output_format: str
    ) -> None:
        super().__init__(configuration, output_format)
        self._api_client = ryax_sdk.ApiClient(configuration)

        # Sub group API
        self.module = RepositoryModules(configuration, output_format)

    def create(
        self,
        name: str,
        url: str,
        username: Optional[str] = None,
        password: Optional[str] = None,
    ) -> Union[str, None]:
        """
        Create a new repository.

        name : name of the repository.
        url : url of repository.
        """
        api_instance = RepositoriesApi(self._api_client)
        body = RepositoryAddSource(
            name=name,
            url=url,
            username=username,
            password=password,
        )

        # Add source
        res = api_instance.repository_sources_post(body=body)

        if self._output_format_is_human():
            print(f"Repository {name} was created with id {res.id}")
            return None
        else:
            return self._print_formatted(res.to_dict())

    def list(self) -> Union[str, None]:
        """
        List all repositories
        """
        api_instance = RepositoriesApi(self._api_client)
        res: List[RepositorySource] = api_instance.repository_sources_get()

        res_dict = [res_dict.to_dict() for res_dict in res]

        if self._output_format_is_human():
            print_list_of_dict_as_table(res_dict, ["id", "name", "url"], sort_by="name")
            return None
        return self._print_formatted(res_dict)

    def get(self, source_id: str) -> Union[str, None]:
        """
        Show a repository by id
        """
        api_instance = RepositoriesApi(self._api_client)
        res: RepositorySource = api_instance.repository_sources_source_id_get(source_id)
        if self._output_format_is_human():
            print_list_of_dict_as_table(
                [res.to_dict()], ["id", "name", "url"], sort_by="name"
            )
            return None
        return self._print_formatted(res.to_dict())

    def update(
        self, source_id: str, name: Optional[str] = None, url: Optional[str] = None
    ) -> Union[str, None]:
        """
        Update repository fields
        """
        api_instance = RepositoriesApi(self._api_client)
        body = RepositoryUpdateSource(name=name, url=url)
        res: RepositorySource = api_instance.repository_sources_source_id_put(
            source_id, body=body
        )
        if self._output_format_is_human():
            print(f"Repository {res.id} updated.")
            return None
        return self._print_formatted(res.to_dict())

    def delete(self, source_id: str) -> None:
        """
        Delete a repository
        """
        api_instance = RepositoriesApi(self._api_client)
        api_instance.repository_sources_source_id_delete(source_id)
        if self._output_format_is_human():
            print(f"The repository '{source_id}' was deleted.")

    def scan(
        self,
        source_id: str,
        branch: Optional[str] = None,
    ) -> Union[str, None]:
        """
        Scan a repository and display list of modules.
        """
        api_instance = RepositoriesApi(self._api_client)
        body = RepositorySourceScanRequest(
            branch=branch,
        )

        # Scan source
        res: RepositoryRepositoryScan = (
            api_instance.repository_sources_source_id_scan_post(source_id, body=body)
        )

        if self._output_format_is_human():
            if res.error:
                print_error(f"Error while scanning the repository:\n{res.error}")
                exit(1)
            else:
                print_list_of_dict_as_table(
                    [module.to_dict() for module in res.modules],
                    ["module_id", "name", "version", "status"],
                    sort_by="name",
                )
            return None
        return self._print_formatted(res.to_dict())
