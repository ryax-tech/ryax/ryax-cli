#!/usr/bin/env python3
# Copyright (C) Ryax Technologies
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# FIXME supress InsecureRequestWarning, need to add a signing authority

from __future__ import print_function

import getpass
import json
import mimetypes
import os
import sys
from operator import itemgetter
from typing import Any, BinaryIO, Dict, Iterator, List, Optional, TextIO, Tuple, Union

import requests
import yaml
from requests.structures import CaseInsensitiveDict

import ryax_sdk

RYAX_API_SUPPORTED_VERSION = ["ryax.tech/v1alpha5", "ryax.tech/v1"]
RYAX_MODULE_TYPES = ["Functions", "Gateways", "StreamingOperators", "Publishers"]


class APIError(Exception):
    pass


class TCOLOR:
    HEADER = "\033[95m"
    OKBLUE = "\033[94m"
    OKGREEN = "\033[92m"
    WARNING = "\033[93m"
    FAIL = "\033[91m"
    BOLD = "\033[1m"
    UNDERLINE = "\033[4m"
    ENDC = "\033[0m"


def print_color(color: str, msg: str, output: TextIO = sys.stdout) -> None:
    """Console print in the wanted color. The given color MUST match a TCOLOR attibute"""
    print(f"{color}{msg}{TCOLOR.ENDC}", file=output)


def print_ok(msg: str) -> None:
    print_color(TCOLOR.OKGREEN, msg)


def print_fail(msg: str) -> None:
    print_color(TCOLOR.FAIL, msg)


def print_wait(msg: str) -> None:
    print_color(TCOLOR.OKBLUE, msg)


def print_header(msg: str) -> None:
    """Console print in error style"""
    print_color(TCOLOR.HEADER, msg)


def print_error(msg: str) -> None:
    """Console print in error style"""
    print_color(TCOLOR.FAIL, msg, output=sys.stderr)


def print_warning(msg: str) -> None:
    """Console print in warning style"""
    print_color(TCOLOR.WARNING, msg, output=sys.stderr)


def confirm(prompt: Optional[str] = None, resp: bool = False) -> bool:
    """
    Prompts for yes or no response from the user. Returns True for yes and
    False for no.

    'resp' should be set to the default value assumed by the caller when user
    simply types ENTER.

    From: http://code.activestate.com/recipes/541096-prompt-the-user-for-confirmation/

    >>> confirm(prompt='Create Directory?', resp=True)
    Create Directory? [y]|n:
    True
    >>> confirm(prompt='Create Directory?', resp=False)
    Create Directory? [n]|y:
    False
    >>> confirm(prompt='Create Directory?', resp=False)
    Create Directory? [n]|y: y
    True
    """

    if prompt is None:
        prompt = "Confirm"

    if resp:
        prompt = "%s [%s]|%s: " % (prompt, "y", "n")
    else:
        prompt = "%s [%s]|%s: " % (prompt, "n", "y")

    while True:
        ans = input(prompt)
        if not ans:
            return resp
        if ans not in ["y", "Y", "n", "N"]:
            print("please enter y or n.")
            continue
        if ans == "y" or ans == "Y":
            return True
        if ans == "n" or ans == "N":
            return False


def ask_password() -> str:
    return getpass.getpass()


def stringlist2intervals_generator(str_list: List[str]) -> Iterator[int]:
    prev = int(str_list[0])
    in_interval = False
    for i_s in str_list[1:]:
        i = int(i_s)
        if in_interval:
            if prev + 1 == i:
                prev = i
                continue
            # The interval end at prev
            in_interval = False
            yield prev
            yield i
        else:
            if prev + 1 == i:
                in_interval = True
                yield prev
            else:
                yield i
        prev = i


def stringlist2intervals(str_list: List[str]) -> Iterator[str]:
    """
    Transform a list of string representing integer, e.g.:

    >>> stringlist2intervals(['139', '140', '141', '150'])
    ['139-141', '150']
    """
    prev = int(str_list[0])
    in_interval = len(str_list) > 1 and prev + 1 == int(str_list[1])
    yield str_list[0]
    if in_interval:
        yield "-"
    for i_s in str_list[1:]:
        i = int(i_s)
        if in_interval:
            if prev + 1 == i:
                prev = i
                continue
            # The interval end at prev
            in_interval = False
            yield str(prev)
            yield ","
            yield i_s
        else:
            if prev + 1 == i:
                in_interval = True
                yield "-"
            else:
                yield ","
                yield i_s
        prev = i
    if in_interval:
        yield i_s


def print_list_of_dict_as_table(
    d: List,
    headers_order: Optional[List[str]] = None,
    sort_by: Optional[str] = None,
    max_value_len: int = 80,
) -> None:
    """
    print the list of dict <d> as a table in the console.  Items will be sorted
    using [<sort_by>] + <headers_order> and only items that are in <headers_order> are
    printed.
    """

    def get(obj: Any, attr: str) -> Any:
        return getattr(obj, attr, obj[attr])

    def set(obj: Any, attr: str, value: Any) -> None:
        try:
            obj.attr = value
        except AttributeError:
            obj[attr] = value

    if headers_order is None:
        if len(d) == 0:
            return
        else:
            headers_order = list(d[0].keys())

    sorted(d, key=itemgetter(*headers_order))
    if sort_by is not None:
        d = sorted(d, key=lambda x: (x[sort_by] or "?"))

    # convert non string values to strings
    for row in d:
        for h in headers_order:
            if type(get(row, h)) == list:
                if (
                    len(get(row, h)) > 0
                    and type(get(row, h)[0]) == str
                    and get(row, h)[0].isdigit()
                ):
                    set(row, h, "".join(stringlist2intervals(get(row, h))))
                else:
                    set(row, h, ",".join([str(x) for x in get(row, h)]))
            elif not isinstance((get(row, h)), str):
                set(row, h, str(get(row, h)))
            # limit row size
            if len(get(row, h)) > max_value_len:
                set(row, h, get(row, h)[: (max_value_len - 3)] + "...")

    headers_width = {}
    for h in headers_order:
        headers_width[h] = len(h)
        for row in d:
            headers_width[h] = max(headers_width[h], len(str(get(row, h))))

    print(TCOLOR.UNDERLINE + "|", end="")
    for h in headers_order:
        print(" {: >{}}|".format(h, headers_width[h]), end="")
    print(TCOLOR.ENDC)

    for row in d:
        print("|", end="")
        for h in headers_order:
            print(" {: >{}}|".format(get(row, h), headers_width[h]), end="")
        print("")


class RyaxdConnexion(object):
    def __init__(
        self,
        srv_url: str,
        srv_verify: bool = True,
        session_file: str = "~/.ryax_session.json",
    ) -> None:
        self.server = self.handle_server_url(srv_url)
        self.session_file = os.path.expanduser(session_file)
        self.session_tokens: dict = {}
        self.jwt_token: dict = {}
        self.session_restore(ask_user=False)
        self.verify: bool = srv_verify

    @staticmethod
    def handle_server_url(server_url: str) -> str:
        if not server_url.startswith("http"):
            raise Exception(
                f"Only HTTP and HTTPS protocols are supported, url: {server_url}"
            )
        elif server_url.endswith("/"):
            server_url = server_url[:-1]
        if server_url.endswith("/api"):
            server_url = server_url[:-4]
        return server_url

    def login(
        self, username: Optional[str] = None, password: Optional[str] = None
    ) -> dict:
        if username is None:
            try:
                username = input("Username: ")
            except KeyboardInterrupt:
                print("\nAbort!")
                exit()
        if password is None:
            try:
                password = ask_password()
            except KeyboardInterrupt:
                print("\nAbort!")
                exit()
        r = requests.post(
            f"{self.server}/authorization/login",
            headers={"content-type": "application/json"},
            data=json.dumps({"username": username, "password": password}),
            verify=self.verify,
        )
        if r.status_code != 200:
            print(self.return_request(r))
            exit()
        t = self.return_request(r)
        self.session_store(t)
        return t

    def logout(self) -> Dict[str, str]:
        self.load_session_tokens()
        self.session_tokens.pop(self.server, None)
        if not self.session_tokens:
            os.remove(self.session_file)
        else:
            self.save_session_tokens()
        return {"status": "DONE", "detail": "JWT token was removed"}

    def load_session_tokens(self) -> None:
        if os.path.exists(self.session_file):
            with open(self.session_file, "r") as f:
                self.session_tokens = json.load(f)

    def save_session_tokens(self) -> None:
        with open(self.session_file, "w") as f:
            json.dump(self.session_tokens, f, sort_keys=True, indent=4)

    def wipe_current_session_from_session_file(self) -> None:
        self.load_session_tokens()
        if self.server in self.session_tokens:
            self.session_tokens.pop(self.server)
            self.save_session_tokens()

    def session_store(self, r: Dict[str, str]) -> None:
        self.load_session_tokens()
        self.jwt_token["authorization"] = r["jwt"]
        self.session_tokens[self.server] = self.jwt_token
        self.save_session_tokens()

    def session_restore(self, ask_user: bool = True) -> None:
        if self.jwt_token.get("authorization") is None:
            session_token_set = False
            self.load_session_tokens()
            if self.server in self.session_tokens:
                if self.session_tokens[self.server].get("authorization") is not None:
                    self.jwt_token["authorization"] = self.session_tokens[
                        self.server
                    ].get("authorization")
                    session_token_set = True
            if ask_user and not session_token_set:
                self.login()

    def session_check(self, req: requests.Response) -> None:
        if req.status_code == 401:
            self.wipe_current_session_from_session_file()
            self.jwt_token = {}

    def _construct_upload_request(
        self, jsondata: dict, files_to_upload: List[str]
    ) -> List[Tuple[str, Tuple[str, Union[str, BinaryIO], str]]]:
        files_req: List[Tuple[str, Tuple[str, Union[str, BinaryIO], str]]] = []
        i = 0
        for f in files_to_upload:
            t = mimetypes.guess_type(f)[0]
            if t is None:
                t = "application/octet-stream"
            files_req.append(("file" + str(i), (os.path.basename(f), open(f, "rb"), t)))
            i += 1
        files_req.append(
            ("RYAX_REQUEST", ("RYAX_REQUEST", json.dumps(jsondata), "application/json"))
        )
        return files_req

    def return_request(self, r: requests.Response) -> Any:
        self.session_check(r)
        if r.status_code != 200 and r.status_code != 201:
            raise APIError(
                {
                    "title": f"Requests returns an HTTP error: {r.status_code}",
                    "detail": r.text,
                    "inline": {"Raw Data": str(r.content)},
                    "url": r.url,
                }
            )
        content = r.json()
        return content

    def get(self, url: str, should_be_logged: bool = True) -> Any:
        if should_be_logged:
            self.session_restore()
        r = requests.get(
            self.server + url,
            headers={"content-type": "application/json", **self.jwt_token},
            verify=self.verify,
        )
        return self.return_request(r)

    def get_with_headers(
        self, url: str, should_be_logged: bool = True
    ) -> Tuple[CaseInsensitiveDict, Any]:
        if should_be_logged:
            self.session_restore()
        r = requests.get(
            f"{self.server}{url}",
            headers={"content-type": "application/json", **self.jwt_token},
            verify=self.verify,
        )
        return r.headers, self.return_request(r)

    def post(
        self,
        url: str,
        jsondata: Dict = {},
        files_to_upload: Optional[List[str]] = None,
        should_be_logged: bool = True,
    ) -> Any:
        if should_be_logged:
            self.session_restore()
        if files_to_upload is None:
            r = requests.post(
                f"{self.server}{url}",
                headers={"content-type": "application/json", **self.jwt_token},
                data=json.dumps(jsondata),
                verify=self.verify,
            )
        else:
            if type(files_to_upload) != list:
                raise Exception("files_to_upload should be a list")
            files_req = self._construct_upload_request(jsondata, files_to_upload)
            r = requests.post(
                f"{self.server}{url}",
                files=files_req,
                headers=self.jwt_token,
                verify=self.verify,
            )
        return self.return_request(r)

    def delete(self, url: str, should_be_logged: bool = True) -> Dict[str, Any]:
        if should_be_logged:
            self.session_restore()
        r = requests.delete(
            f"{self.server}{url}",
            headers={"content-type": "application/json", **self.jwt_token},
            verify=self.verify,
        )
        return self.return_request(r)

    def patch(
        self, url: str, jsondata: dict, should_be_logged: bool = True
    ) -> Dict[str, Any]:
        if should_be_logged:
            self.session_restore()
        r = requests.patch(
            f"{self.server}{url}",
            headers={"content-type": "application/json", **self.jwt_token},
            data=json.dumps(jsondata),
            verify=self.verify,
        )
        return self.return_request(r)

    def put(
        self, url: str, jsondata: dict, should_be_logged: bool = True
    ) -> Dict[str, Any]:
        if should_be_logged:
            self.session_restore()
        r = requests.put(
            f"{self.server}{url}",
            headers={"content-type": "application/json", **self.jwt_token},
            data=json.dumps(jsondata),
            verify=self.verify,
        )
        return self.return_request(r)

    def _add_file_to_request(self, files: List, file_path: str, part_name: str) -> None:
        t = mimetypes.guess_type(file_path)[0]
        if t is None:
            t = "application/octet-stream"
        files.append(
            (part_name, (os.path.basename(file_path), open(file_path, "rb"), t))
        )


def get_configuration(
    conn: RyaxdConnexion, debug: bool = False
) -> ryax_sdk.Configuration:
    """Create an initialized API configuration"""
    configuration = ryax_sdk.Configuration(host=conn.server)
    configuration.debug = debug
    configuration.verify_ssl = conn.verify
    conn.session_restore()
    configuration.api_key["bearer"] = conn.jwt_token["authorization"]
    return configuration


class BaseCLI(object):
    def __init__(self, output_format: str) -> None:
        self._output_format = output_format

        # import ipdb; ipdb.set_trace()

    def _output_format_is_human(self) -> bool:
        return self._output_format.upper() == "HUMAN"

    def _print_formatted(self, output: Any) -> str:
        """
        return the formated output in a the given format:
        - human
        - json
        - yaml
        """
        if self._output_format.upper() in ("JSON", "HUMAN"):
            output = json.dumps(output, indent=2, default=str)
        elif self._output_format.upper() == "YAML":
            output = yaml.dump(output, default_flow_style=False)
        return output

    def _print_wf_deploy(self, data: Any) -> None:
        print(">", data, "\n")


class BaseCLIWithConnection(BaseCLI):
    def __init__(self, conn: RyaxdConnexion, output_format: str) -> None:
        super().__init__(output_format)
        self._conn = conn


class BaseCLIWithConfig(BaseCLI):
    def __init__(
        self, configuration: ryax_sdk.Configuration, output_format: str
    ) -> None:
        super().__init__(output_format)
        self._configuration = configuration
