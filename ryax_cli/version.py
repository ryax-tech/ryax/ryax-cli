import platform

try:
    import importlib.metadata as importlib

    try:
        __version__ = importlib.version("ryax_cli")
    except NameError:
        __version__ = "dev"
except ModuleNotFoundError:
    import importlib_metadata

    try:
        __version__ = importlib_metadata.version("ryax_cli")
    except NameError:
        __version__ = "dev"

python_version = platform.python_version()
