# Copyright (C) Ryax Technologies
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import json
import logging
import shutil
import sys
import tempfile
from io import BufferedReader
from pathlib import Path
from typing import Any, Dict, List, Optional

import yaml

import ryax_sdk
from ryax_sdk.api.workflow_actions_static_file_input_api import (
    WorkflowActionsStaticFileInputApi,
)
from ryax_sdk.api.workflow_module_outputs_api import WorkflowModuleOutputsApi
from ryax_sdk.api.workflow_modules_api import WorkflowModulesApi
from ryax_sdk.api.workflow_modules_inputs_api import WorkflowModulesInputsApi
from ryax_sdk.api.workflow_modules_outputs_api import WorkflowModulesOutputsApi
from ryax_sdk.api.workflows_api import WorkflowsApi
from ryax_sdk.model.studio_add_workflow_module import StudioAddWorkflowModule
from ryax_sdk.model.studio_add_workflow_module_dynamic_output import (
    StudioAddWorkflowModuleDynamicOutput,
)
from ryax_sdk.model.studio_add_workflow_request import StudioAddWorkflowRequest
from ryax_sdk.model.studio_add_workflow_response import StudioAddWorkflowResponse
from ryax_sdk.model.studio_update_workflow_module_input_value import (
    StudioUpdateWorkflowModuleInputValue,
)
from ryax_sdk.model.studio_update_workflow_request import StudioUpdateWorkflowRequest
from ryax_sdk.model.studio_workflow_extended import StudioWorkflowExtended
from ryax_sdk.model.studio_workflow_module_input import StudioWorkflowModuleInput
from ryax_sdk.model.studio_workflow_module_output import StudioWorkflowModuleOutput
from ryax_sdk.model.studio_workflow_validation import StudioWorkflowValidation

from .librutils import (
    RYAX_API_SUPPORTED_VERSION,
    BaseCLIWithConfig,
    print_list_of_dict_as_table,
)

LOG = logging.getLogger(__name__)


class WorkflowModuleOutputs(BaseCLIWithConfig):
    """
    Handle Workflow Module Outputs operations.
    """

    def __init__(
        self, configuration: ryax_sdk.Configuration, output_format: str = "json"
    ) -> None:
        super().__init__(configuration, output_format)
        self._api_client = ryax_sdk.ApiClient(configuration)

    def list(
        self,
        workflow_id: str,
        with_type: Optional[str] = None,
        accessible_from: Optional[str] = None,
    ) -> Optional[str]:
        """
        List workflow module outputs.
        """
        api_instance = WorkflowModuleOutputsApi(self._api_client)
        res: List[
            StudioWorkflowModuleOutput
        ] = api_instance.studio_workflows_workflow_id_modules_outputs_get(
            workflow_id, with_type=with_type, accessible_from=accessible_from
        )

        res_dict = [res_dict.to_dict() for res_dict in res]
        if self._output_format_is_human():
            print_list_of_dict_as_table(
                res_dict,
                ["id", "name", "kind", "version", "description"],
                sort_by="name",
            )
            return None
        return self._print_formatted(res_dict)

    def add(self, workflow_id: str, workflow_module_id: str, output_def: str) -> None:
        """
        Add an output to the workflow module.

        output_def: should be a json formated string of format defined in:
        https://gitlab.com/ryax-tech/dev/ryax-sdk/ryax-python-sdk/-/blob/master/docs/RyaxStudioAddWorkflowModuleDynamicOutput.md
        """
        output_def_dict = json.loads(output_def)
        body = StudioAddWorkflowModuleDynamicOutput(**output_def_dict)
        api_instance = WorkflowModulesOutputsApi(self._api_client)
        api_instance.studio_workflows_workflow_id_modules_workflow_module_id_outputs_post(
            workflow_id, workflow_module_id, body=body
        )
        if self._output_format_is_human():
            print("Output created succesfully")


class WorkflowModuleInputs(BaseCLIWithConfig):
    """
    Handle Workflow Module Inputs operations.
    """

    def __init__(
        self, configuration: ryax_sdk.Configuration, output_format: str = "json"
    ) -> None:
        super().__init__(configuration, output_format)
        self._api_client = ryax_sdk.ApiClient(configuration)
        self._configuration = configuration

    def list(
        self,
        workflow_id: str,
        module_id: str,
    ) -> Optional[str]:
        """
        List workflow module outputs.
        """
        api_instance = WorkflowModulesInputsApi(self._api_client)
        res: List[
            StudioWorkflowModuleInput
        ] = api_instance.studio_workflows_workflow_id_modules_workflow_module_id_inputs_get(
            workflow_id, module_id
        )

        res_dict = [res_dict.to_dict() for res_dict in res]
        if self._output_format_is_human():
            print_list_of_dict_as_table(
                res_dict,
                ["id", "type", "technical_name", "display_name"],
                sort_by="technical_name",
            )
            return None
        return self._print_formatted(res_dict)

    def set(
        self,
        workflow_id: str,
        workflow_module_id: str,
        workflow_input_id: str,
        value: Any,
    ) -> None:
        """
        Set a workflow module input value.
        """
        api_instance = WorkflowModulesInputsApi(self._api_client)

        if isinstance(value, str) and value.startswith("="):
            # this is a reference value, get the associated output id
            body = StudioUpdateWorkflowModuleInputValue(reference_value=value)
        else:
            all_inputs: List[
                StudioWorkflowModuleInput
            ] = api_instance.studio_workflows_workflow_id_modules_workflow_module_id_inputs_get(
                workflow_id, workflow_module_id
            )
            """
            help (str): Workflow module input help. [optional]  # noqa: E501
            reference_output (StudioWorkflowModuleInputReferenceOutput): [optional]  # noqa: E501
            technical_name (str): Workflow module input technical name. [optional]  # noqa: E501
            type (str): Module input type. [optional]  # noqa: E501
            reference_value (str, none_type): Module input value to reference other output. [optional]  # noqa: E501
            enum_values ([str]): Module input available options for enum type. [optional]  # noqa: E501
            workflow_file (StudioWorkflowModuleInputWorkflowFile): [optional]  # noqa: E501
            id (str): ID of the workflow module input. [optional]  # noqa: E501
            display_name (str): Workflow module input display name. [optional]  # noqa: E501
            static_value (str, none_type): Module input value to reference other input. [optional]  # noqa: E501"""
            the_input: Optional[StudioWorkflowModuleInput] = None
            for i in all_inputs:
                if i.id == workflow_input_id:
                    the_input = i
            if the_input is None:
                raise ValueError("Unknown input.")

            if the_input.type == "file":
                if Path(value).is_absolute():
                    input_file_path = Path(value)
                else:
                    input_file_path = Path.cwd() / Path(value)

                if not input_file_path.is_file():
                    raise ValueError("Please, provide a file.")

                if self._output_format_is_human():
                    print(f"\t\tUploading '{input_file_path}'...")
                api_file = WorkflowActionsStaticFileInputApi(self._api_client)
                api_file.studio_workflows_workflow_id_modules_module_id_inputs_workflow_input_id_file_post(
                    workflow_id,
                    workflow_module_id,
                    workflow_input_id,
                    open(input_file_path, "rb"),
                )
            if the_input.type == "directory":
                if Path(value).is_absolute():
                    input_file_path = Path(value)
                else:
                    input_file_path = Path.cwd() / Path(value)

                if not input_file_path.is_file() or input_file_path.suffix != ".zip":
                    raise ValueError("To set a directory, give a zip file.")

                if self._output_format_is_human():
                    print(f"\t\tUploading '{input_file_path}'...")
                api_file = WorkflowActionsStaticFileInputApi(self._api_client)
                api_file.studio_workflows_workflow_id_modules_module_id_inputs_workflow_input_id_file_post(
                    workflow_id,
                    workflow_module_id,
                    workflow_input_id,
                    open(input_file_path, "rb"),
                )

            else:  # This is a static values (string/int/float/...)
                body = StudioUpdateWorkflowModuleInputValue(static_value=value)
                # Do send the update
                api_instance.studio_workflows_workflow_id_modules_workflow_module_id_inputs_workflow_input_id_value_put(
                    workflow_id, workflow_module_id, workflow_input_id, body=body
                )
        if self._output_format_is_human():
            print(
                f"Input value {workflow_input_id} of module {workflow_module_id} set to '{value}'"
            )


class WorkflowModules(BaseCLIWithConfig):
    """
    Handle Workflow Modules operations.
    """

    def __init__(
        self, configuration: ryax_sdk.Configuration, output_format: str
    ) -> None:
        super().__init__(configuration, output_format)
        self._api_client = ryax_sdk.ApiClient(configuration)

        # Sub-group API
        self.output = WorkflowModuleOutputs(configuration, output_format)
        self.input = WorkflowModuleInputs(configuration, output_format)

    def add(
        self,
        workflow_id: str,
        module_id: str,
        position_x: int,
        position_y: int,
        custom_name: Optional[str] = None,
    ) -> None:
        """
        Add a module to the workflow.
        """
        body = StudioAddWorkflowModule(
            custom_name=custom_name,
            module_id=module_id,
            position_x=position_x,
            position_y=position_y,
        )
        api_instance = WorkflowModulesApi(self._api_client)

        api_instance.studio_workflows_workflow_id_modules_post(workflow_id, body=body)

        if self._output_format_is_human():
            print(f"Module '{module_id}' was added to the workflow '{workflow_id}'")


class Workflow(BaseCLIWithConfig):
    """
    Handle workflow operations.
    """

    def __init__(
        self, configuration: ryax_sdk.Configuration, output_format: str = "json"
    ) -> None:
        super().__init__(configuration, output_format)
        self._api_client = ryax_sdk.ApiClient(configuration)
        self._configuration = configuration

        # Sub group API
        self.module = WorkflowModules(configuration, output_format)

    def list(self, search: Optional[str] = None) -> Optional[str]:
        """
        List all workflows.
        """
        api_instance = WorkflowsApi(self._api_client)

        if search is None:
            res = api_instance.studio_workflows_get()
        else:
            res = api_instance.studio_workflows_get(search=search)

        res_dict = [res_dict.to_dict() for res_dict in res]

        if self._output_format_is_human():
            print_list_of_dict_as_table(
                res_dict,
                ["id", "name", "description", "status", "deployment_status"],
                sort_by="name",
            )
            return None
        return self._print_formatted(res_dict)

    def get(self, workflow_id: str) -> Optional[str]:
        """
        Get workflow details.
        """

        api_instance = WorkflowsApi(self._api_client)
        res: StudioWorkflowExtended = api_instance.studio_v2_workflows_workflow_id_get(
            workflow_id
        )

        if self._output_format_is_human():
            wf = res.to_dict()
            print(wf["name"])
            print("id:", wf["id"])
            deploy_status = ""
            if wf["deployment_status"] != "None":
                deploy_status = f" ({wf['deployment_status']})"
            print(f'Status: {wf["status"]}' + deploy_status)
            print("")
            print("Modules:")
            for f in wf["modules"]:
                print(
                    f'  - {f["custom_name"] if f["custom_name"] is not None else f["name"]} ({f["module_id"]})'
                )
            return None
        return self._print_formatted(res.to_dict())

    def create(self, *workflow_file_paths: str) -> Optional[str]:
        """
        Create a new workflow draft based on file contaning a yaml definition.
        """
        for workflow_file_path in workflow_file_paths:
            data_raw = yaml.safe_load(open(workflow_file_path))
            if data_raw["apiVersion"] not in RYAX_API_SUPPORTED_VERSION:
                print(f"This tool only supports {RYAX_API_SUPPORTED_VERSION} objects")
                sys.exit(1)
            if data_raw["kind"] != "Workflows":
                print("This command only support workflows resource")
                sys.exit(2)

            api_instance = WorkflowsApi(self._api_client)

            # Validate the workflow
            resp_valid: StudioWorkflowValidation = (
                api_instance.studio_workflows_schema_validate_post(workflow_file_path)
            )
            if resp_valid.status != "Valid":
                print(
                    f"The workflow definition of {workflow_file_path} is invalid: {resp_valid.message}"
                )
                sys.exit(3)

            workflow_def = data_raw

            # Get files to upload
            to_upload: Dict[str, Path] = {}
            # import pdb pdb.set_trace()
            for function in data_raw["spec"]["functions"]:
                input_values: Dict[str, str] = function.get("inputs_values", {})
                for input_name, input_val in input_values.items():
                    path = Path(input_val)
                    if path.is_absolute():
                        input_file_path = path
                    else:
                        # Resolve the Path relatively to the workflow
                        # definition
                        input_file_path = Path(workflow_file_path).parent / input_val
                    if not input_file_path.is_file():
                        # This is not a file
                        LOG.debug(
                            f"Input name {input_name} with value '{input_val}' is not a valid file"
                        )
                        continue

                    input_file_id = (
                        function["id"] + "-" + input_name + input_file_path.name
                    )
                    to_upload[input_file_id] = input_file_path
                    # Change the path off the file in the definition
                    input_values[input_name] = input_file_id

            with tempfile.TemporaryDirectory() as to_export_dir:
                export_dir_path = Path(to_export_dir)

                # Copy static files
                for input_id, input_path in to_upload.items():
                    shutil.copy(input_path, export_dir_path / input_id)

                # Copy the workflow
                with open(export_dir_path / "workflow.yaml", mode="w") as wf_file:
                    yaml.dump(workflow_def, wf_file)

                # List archive content
                if self._output_format_is_human():
                    file_list = list(export_dir_path.glob("*"))
                    LOG.debug(f"List of the files to be uploaded: {file_list}")

                # Create the Zip file
                workflow_packaging_file = shutil.make_archive(
                    str(export_dir_path), "zip", export_dir_path
                )
                LOG.debug("ZIP file created: %s", workflow_packaging_file)

                # Upload
                resp: StudioAddWorkflowResponse = (
                    api_instance.studio_workflows_import_post(workflow_packaging_file)
                )

                if self._output_format_is_human():
                    print(f"Workflow {resp.workflow_id} created!")
                else:
                    return self._print_formatted(resp)
        return None

    def copy(
        self,
        name: str,
        from_workflow: str,
        description: Optional[str] = None,
    ) -> Optional[str]:
        """
        Copy an existing workflow.

        from_workflow: another existing workflow id to copy
        descrition: custom description
        """
        body = StudioAddWorkflowRequest(
            description=description,
            name=name,
            from_workflow=from_workflow,
        )
        api_instance = WorkflowsApi(self._api_client)

        res = api_instance.studio_workflows_post(body=body)

        if self._output_format_is_human():
            print(f"Workflow '{name}' created with id '{res.workflow_id}'.")
            return None
        return self._print_formatted(res.to_dict())

    def update(
        self,
        workflow_id: str,
        name: Optional[str] = None,
        description: Optional[str] = None,
    ) -> Optional[str]:
        """
        Update a workflow.

        name: new name for the workflow
        descrition: custom description
        """
        body = StudioUpdateWorkflowRequest(
            description=description,
            name=name,
        )
        api_instance = WorkflowsApi(self._api_client)

        res = api_instance.studio_workflows_workflow_id_put(workflow_id, body=body)

        if self._output_format_is_human():
            print(f"Workflow '{res.workflow_id}' updated.")
            return None
        return res

    def deploy(self, *workflow_ids: str) -> Optional[str]:
        """
        Deploy one or more workflow.
        """
        ret = ""
        api_instance = WorkflowsApi(self._api_client)
        for workflow_id in workflow_ids:
            res = api_instance.studio_workflows_workflow_id_deploy_post(workflow_id)
            if self._output_format_is_human():
                print(f"Deployment of '{workflow_id}' triggered")
            else:
                ret += self._print_formatted(res)
        return ret

    def stop(self, *workflow_ids: str) -> Optional[str]:
        """
        Stop a workflow.
        """
        ret = ""
        api_instance = WorkflowsApi(self._api_client)
        for workflow_id in workflow_ids:
            res = api_instance.studio_workflows_workflow_id_stop_post(workflow_id)
            if self._output_format_is_human():
                print(f"Undeployment of '{workflow_id}' triggered.")
            else:
                ret += self._print_formatted(res)
        return ret

    def delete(self, *workflow_ids: List[str]) -> Optional[str]:
        """
        Delete one or more workflows.
        """
        api_instance = WorkflowsApi(self._api_client)
        ret = ""
        for workflow_id in workflow_ids:
            res = api_instance.studio_workflows_workflow_id_delete(workflow_id)
            if self._output_format_is_human():
                print(f"The workflow '{workflow_id}' was deleted.")
            else:
                ret += self._print_formatted(res)
        return ret

    def download(self, workflow_id: str, export_dir: str, unzip: bool = False) -> None:
        """
        Download (export) a workflow
        """
        api_instance = WorkflowsApi(self._api_client)
        workflow_zip: BufferedReader = (
            api_instance.studio_workflows_workflow_id_export_get(workflow_id)
        )
        if unzip is True:
            with tempfile.NamedTemporaryFile(suffix=".zip") as workflow_zip_file:
                workflow_zip_file.write(workflow_zip.read())
                workflow_zip_file.flush()
                shutil.unpack_archive(workflow_zip_file.name, export_dir)
        else:
            Path(export_dir).mkdir(parents=True, exist_ok=True)
            with open(Path(export_dir) / (workflow_id + ".zip"), "wb") as zip_file:
                zip_file.write(workflow_zip.read())
                zip_file.flush()

        if self._output_format_is_human():
            print(f"The workflow '{workflow_id}' is available in {export_dir}")

    def upload(self, workflow_export_zip_path: str) -> None:
        """
        Download (export) a workflow
        """
        api_instance = WorkflowsApi(self._api_client)
        with open(
            Path(workflow_export_zip_path), mode="rb"
        ) as workflow_export_zip_file:
            response: dict = api_instance.studio_workflows_import_post(
                workflow_export_zip_file
            )
        if self._output_format_is_human():
            print(
                f"The workflow '{response['workflow_id']}' was created from {workflow_export_zip_path}"
            )
