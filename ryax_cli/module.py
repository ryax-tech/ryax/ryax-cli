#!/usr/bin/env python3
# Copyright (C) Ryax Technologies
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

from typing import Union

import ryax_sdk
from ryax_sdk.api.modules_api import ModulesApi
from ryax_sdk.model.studio_module_details import StudioModuleDetails

from .librutils import BaseCLIWithConfig, print_list_of_dict_as_table


class Module(BaseCLIWithConfig):
    """
    Handle modules operations.
    """

    def __init__(
        self, configuration: ryax_sdk.Configuration, output_format: str = "json"
    ) -> None:
        super().__init__(configuration, output_format)
        self._api_client = ryax_sdk.ApiClient(configuration)

    def list(self, search: str = "") -> Union[str, None]:
        """
        List all modules.
        """
        api_instance = ModulesApi(self._api_client)
        res = api_instance.studio_modules_get(search=search)

        res_dict = [res_dict.to_dict() for res_dict in res]

        if self._output_format_is_human():
            print_list_of_dict_as_table(
                res_dict,
                ["id", "name", "kind", "version"],
                sort_by="name",
            )
            return None
        return self._print_formatted(res_dict)

    def get(self, module_id: str) -> Union[str, None]:
        """
        Get module details.
        """
        api_instance = ModulesApi(self._api_client)
        res: StudioModuleDetails = api_instance.studio_modules_module_id_get(module_id)
        if self._output_format_is_human():
            func = res.to_dict()
            inputs = []
            outputs = []
            if "inputs" in func:
                for input in func["inputs"]:
                    inputs.append(
                        f'  - {input["display_name"]}\n    type: {input["type"]}'
                    )
            if "outputs" in func:
                for output in func["outputs"]:
                    outputs.append(
                        f'  - {output["display_name"]}\n    type: {output["type"]}'
                    )
            inputs_str = "\n".join(inputs) if inputs else "  None"
            outputs_str = "\n".join(outputs) if outputs else "  None"
            versions_str = ""
            for v in api_instance.studio_modules_module_id_list_versions_get(module_id):
                versions_str += f"  - {v.version}\n    id: {v.id}"

            msg = [
                f'{func["name"]}',
                f'{func["description"]}',
                f'version: {func["version"]}',
                f'id: {func["id"]}',
                f'kind: {func["kind"]}',
                "Inputs: ",
                f"{inputs_str}",
                "Outputs: ",
                f"{outputs_str}",
                "Versions: ",
                f"{versions_str}",
            ]
            print("\n".join(msg))
            return None
        return self._print_formatted(res.to_dict())

    def delete(self, module_id: str) -> None:
        """
        Delete a module.
        """
        api_instance = ModulesApi(self._api_client)
        api_instance.studio_modules_module_id_delete(module_id)
        if self._output_format_is_human():
            print(f"The module '{module_id}' was deleted.")
