# Copyright (C) Ryax Technologies
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import logging

import ryax_sdk
from ryax_sdk.api.executions_api import ExecutionsApi

from .librutils import BaseCLIWithConfig

LOG = logging.getLogger(__name__)


class Executions(BaseCLIWithConfig):
    """
    Handle Workflow Module Outputs operations.
    """

    def __init__(
        self, configuration: ryax_sdk.Configuration, output_format: str = "json"
    ) -> None:
        super().__init__(configuration, output_format)
        self._api_client = ryax_sdk.ApiClient(configuration)

    def list(self) -> None:
        api_instance = ExecutionsApi(self._api_client)

        res_lits = api_instance.runner_workflow_runs_get()

        print(res_lits)
        return None
