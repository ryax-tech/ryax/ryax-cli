from typing import List, Union

import ryax_sdk
from ryax_cli.librutils import BaseCLIWithConfig, print_list_of_dict_as_table
from ryax_sdk.api.projects_api import ProjectsApi
from ryax_sdk.model.authorization_add_project_request import (
    AuthorizationAddProjectRequest,
)
from ryax_sdk.model.authorization_add_user_to_project_request import (
    AuthorizationAddUserToProjectRequest,
)
from ryax_sdk.model.authorization_update_project_request import (
    AuthorizationUpdateProjectRequest,
)
from ryax_sdk.model.authorization_user import AuthorizationUser


class Project(BaseCLIWithConfig):
    """
    Manage project life-cycle: create, delete, rename.
    """

    def __init__(
        self, configuration: ryax_sdk.Configuration, output_format: str
    ) -> None:
        super().__init__(configuration, output_format)
        self._api_client = ryax_sdk.ApiClient(configuration)

    def list(self) -> Union[str, None]:
        api_instance = ProjectsApi(self._api_client)
        res: List[AuthorizationUser] = api_instance.authorization_projects_get()

        res_dict = [res_dict.to_dict() for res_dict in res]

        if self._output_format_is_human():
            print_list_of_dict_as_table(
                res_dict, ["id", "name", "creation_date"], sort_by="id"
            )
            return None
        else:
            return self._print_formatted(res_dict)

    def get(self, project_id: str) -> str:
        """
        Get project details.
        """
        api_instance = ProjectsApi(self._api_client)
        res: AuthorizationUser = api_instance.authorization_projects_project_id_get(
            project_id
        )

        return self._print_formatted(res.to_dict())

    def create(self, name: str) -> None:
        """
        Create new project from name.
        """
        api_instance = ProjectsApi(self._api_client)
        body = AuthorizationAddProjectRequest(name=name)
        api_instance.authorization_projects_post(body=body)

        print(f"project {name} has been created !")

    def rename(self, project_id: str, name: str) -> None:
        """
        Rename project of id to new name.
        """
        print(f'setting "{project_id}" project\'s name to {name}...')

        api_instance = ProjectsApi(self._api_client)
        body = AuthorizationUpdateProjectRequest(name=name)
        api_instance.authorization_projects_project_id_put(project_id, body=body)

        print("done")

    def delete(self, project_id: str) -> None:
        """
        Delete project.
        """
        api_instance = ProjectsApi(self._api_client)
        api_instance.authorization_projects_project_id_delete(project_id)
        print(f'setting "{project_id}" has been deleted')

    def add_user_to_project(self, project_id: str, user_id: str) -> None:
        """
        Add user to project
        """

        api_instance = ProjectsApi(self._api_client)
        body = AuthorizationAddUserToProjectRequest(user_id=user_id)
        api_instance.authorization_projects_project_id_user_post(project_id, body=body)

        print(f"User {user_id} has been add to {project_id}")

    def delete_user_from_project(self, project_id: str, user_id: str) -> None:
        """
        Delete user from project
        """

        api_instance = ProjectsApi(self._api_client)
        api_instance.authorization_projects_project_id_user_user_id_delete(
            project_id=project_id, user_id=user_id
        )

        print(f"User {user_id} has been deleted from {project_id}")
