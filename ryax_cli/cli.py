#!/usr/bin/env python3
# Copyright (C) Ryax Technologies
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import json
import sys

import fire
import requests

import ryax_sdk

from .librutils import APIError, print_error
from .libryaxcli import Cli


def main() -> None:
    """
    Ryax CLI main launcher script.
    """
    try:
        fire.Fire(Cli)
    except ryax_sdk.exceptions.UnauthorizedException:
        print(
            "Error: you are not authorized to do this. Try to re-login and check your user Role."
        )
        sys.exit(2)
    except APIError as err:
        error_msg = err.args[0]
        print(json.dumps(error_msg, indent=2))
        print_error("Error: " + error_msg["title"] + " " + error_msg["detail"])
        if "inline" in error_msg:
            for key in error_msg["inline"].keys():
                print("\t: " + key, end="", file=sys.stderr)
        sys.exit(1)
    except requests.exceptions.ConnectionError as err:
        print(err)
        error_msg = {
            "title": "Connection refused by REST API server",
            "detail": "Check if the server is running and if host:port are correct.",
        }
        print(json.dumps(error_msg, indent=2))
        print_error("Error: " + error_msg["title"] + " " + error_msg["detail"])
        sys.exit(2)
