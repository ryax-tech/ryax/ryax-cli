from typing import List, Optional, Union

import ryax_sdk
from ryax_cli.librutils import (
    BaseCLIWithConfig,
    ask_password,
    confirm,
    print_list_of_dict_as_table,
)
from ryax_sdk.api.users_api import UsersApi
from ryax_sdk.model.authorization_add_user_request import AuthorizationAddUserRequest
from ryax_sdk.model.authorization_update_user_request import (
    AuthorizationUpdateUserRequest,
)
from ryax_sdk.model.authorization_user import AuthorizationUser


class User(BaseCLIWithConfig):
    """
    Handle user operations.
    """

    def __init__(
        self, configuration: ryax_sdk.Configuration, output_format: str
    ) -> None:
        super().__init__(configuration, output_format)
        self._api_client = ryax_sdk.ApiClient(configuration)

    def create(
        self,
        user: str,
        role: str,
        email: str,
        comment: str = "",
        inline_password: str = "",
    ) -> Optional[str]:
        """
        Create a new user.

        USERNAME : username for the new user.
        ROLE : Admin, Developer, User, or Anonymous.
        EMAIL : email address of new user.
        """
        api_instance = UsersApi(self._api_client)
        body = AuthorizationAddUserRequest(
            username=user,
            password=inline_password if inline_password else ask_password(),
            role=role,
            email=email,
            comment=comment,
        )

        # Add source
        res = api_instance.authorization_users_post(body=body)

        if self._output_format_is_human():
            print(f"User {user} created!")
            return None
        else:
            return self._print_formatted(res.to_dict())

    def list(self) -> Union[str, None]:
        """
        List all available users, output may depend on current session user privileges.
        """
        api_instance = UsersApi(self._api_client)
        res: List[AuthorizationUser] = api_instance.authorization_users_get()

        res_dict = [res_dict.to_dict() for res_dict in res]

        if self._output_format_is_human():
            print_list_of_dict_as_table(
                res_dict,
                ["id", "username", "role", "email", "comment"],
                sort_by="username",
            )
            return None
        else:
            return self._print_formatted(res_dict)

    def get(self, user_id: str) -> Union[str, None]:
        """
        Retrieve details of specified user.
        """
        api_instance = UsersApi(self._api_client)
        res: AuthorizationUser = api_instance.authorization_users_user_id_get(user_id)

        if self._output_format_is_human():
            print(f"User: {res}")
            return None
        else:
            return self._print_formatted(res.to_dict())

    def update(self, user_id: str, fields: dict) -> Union[str, None]:
        """
        Update user details.
        """
        api_instance = UsersApi(self._api_client)
        body = AuthorizationUpdateUserRequest(**fields)
        res = api_instance.authorization_users_user_id_put(user_id, body=body)

        if self._output_format_is_human():
            print(f"User {user_id} was updated!")
            return None
        else:
            return self._print_formatted(res.to_dict())

    def delete(self, user_id: str, force: bool = False) -> None:
        """
        Delete user.
        """
        if not force and not confirm(
            f"Are you sure you want to delete user {user_id}?", False
        ):
            print("Abort!")
            return None

        api_instance = UsersApi(self._api_client)
        api_instance.authorization_users_user_id_delete(user_id)

        print(f"User {user_id} was deleted!")
