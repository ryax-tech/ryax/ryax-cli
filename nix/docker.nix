{ dockerTools,
  ryaxBaseImage,
  ryaxBaseImageConfig,
  appDir,
  tag ? "latest",
}:
dockerTools.buildImage {
  name = "ryax-cli";
  inherit tag;
  created = "now";

  fromImage = ryaxBaseImage;

  extraCommands = ''
    mkdir data
    mkdir -p bin

    # Install python environment created by pip prior to this build
    if [ -d ${appDir}/.env ]; then
      cp -r ${appDir}/.env data/.env
    fi

    # Install the app
    cp -r ${appDir}/* data
    cp ${appDir}/ryax-cli bin/ryax-cli
  '';

  config = ryaxBaseImageConfig // {
    EntryPoint = [ "ryax-cli" ];
    Env = [
      "PYTHONPATH=/data/.env:/data"
    ];
  };
}
