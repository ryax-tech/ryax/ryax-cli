.. documentation master file

.. include:: ../README.rst


Contents
========

.. toctree::
   :maxdepth: 3

   apidoc/modules.rst



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
