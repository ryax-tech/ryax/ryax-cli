#!/usr/bin/env bash

set -e

pytest --cov ./ryax_cli --cov-report xml:coverage.xml --cov-report term --cov-config=.coveragerc --cov-branch -ra -v $@
