"""
    Ryax API for all services

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: 23.05.0
    Generated by: https://openapi-generator.tech
"""


import re  # noqa: F401
import sys  # noqa: F401

from ryax_sdk.api_client import ApiClient, Endpoint as _Endpoint
from ryax_sdk.model_utils import (  # noqa: F401
    check_allowed_values,
    check_validations,
    date,
    datetime,
    file_type,
    none_type,
    validate_and_convert_types
)
from ryax_sdk.model.runner_error import RunnerError
from ryax_sdk.model.runner_execution_log import RunnerExecutionLog


class ExecutionLogsApi(object):
    """NOTE: This class is auto generated by OpenAPI Generator
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """

    def __init__(self, api_client=None):
        if api_client is None:
            api_client = ApiClient()
        self.api_client = api_client

        def __runner_workflow_runs_execution_result_id_logs_get(
            self,
            execution_result_id,
            **kwargs
        ):
            """Get an execution log  # noqa: E501

            Get the logs for an execution result with its ID.  # noqa: E501
            This method makes a synchronous HTTP request by default. To make an
            asynchronous HTTP request, please pass async_req=True

            >>> thread = api.runner_workflow_runs_execution_result_id_logs_get(execution_result_id, async_req=True)
            >>> result = thread.get()

            Args:
                execution_result_id (str):

            Keyword Args:
                _return_http_data_only (bool): response data without head status
                    code and headers. Default is True.
                _preload_content (bool): if False, the urllib3.HTTPResponse object
                    will be returned without reading/decoding response data.
                    Default is True.
                _request_timeout (float/tuple): timeout setting for this request. If one
                    number provided, it will be total request timeout. It can also
                    be a pair (tuple) of (connection, read) timeouts.
                    Default is None.
                _check_input_type (bool): specifies if type checking
                    should be done one the data sent to the server.
                    Default is True.
                _check_return_type (bool): specifies if type checking
                    should be done one the data received from the server.
                    Default is True.
                _host_index (int/None): specifies the index of the server
                    that we want to use.
                    Default is read from the configuration.
                async_req (bool): execute request asynchronously

            Returns:
                RunnerExecutionLog
                    If the method is called asynchronously, returns the request
                    thread.
            """
            kwargs['async_req'] = kwargs.get(
                'async_req', False
            )
            kwargs['_return_http_data_only'] = kwargs.get(
                '_return_http_data_only', True
            )
            kwargs['_preload_content'] = kwargs.get(
                '_preload_content', True
            )
            kwargs['_request_timeout'] = kwargs.get(
                '_request_timeout', None
            )
            kwargs['_check_input_type'] = kwargs.get(
                '_check_input_type', True
            )
            kwargs['_check_return_type'] = kwargs.get(
                '_check_return_type', True
            )
            kwargs['_host_index'] = kwargs.get('_host_index')
            kwargs['execution_result_id'] = \
                execution_result_id
            return self.call_with_http_info(**kwargs)

        self.runner_workflow_runs_execution_result_id_logs_get = _Endpoint(
            settings={
                'response_type': (RunnerExecutionLog,),
                'auth': [
                    'bearer'
                ],
                'endpoint_path': '/runner/workflow_runs/{execution_result_id}/logs',
                'operation_id': 'runner_workflow_runs_execution_result_id_logs_get',
                'http_method': 'GET',
                'servers': None,
            },
            params_map={
                'all': [
                    'execution_result_id',
                ],
                'required': [
                    'execution_result_id',
                ],
                'nullable': [
                ],
                'enum': [
                ],
                'validation': [
                ]
            },
            root_map={
                'validations': {
                },
                'allowed_values': {
                },
                'openapi_types': {
                    'execution_result_id':
                        (str,),
                },
                'attribute_map': {
                    'execution_result_id': 'execution_result_id',
                },
                'location_map': {
                    'execution_result_id': 'path',
                },
                'collection_format_map': {
                }
            },
            headers_map={
                'accept': [
                    'application/json'
                ],
                'content_type': [],
            },
            api_client=api_client,
            callable=__runner_workflow_runs_execution_result_id_logs_get
        )
