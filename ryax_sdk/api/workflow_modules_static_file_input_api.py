"""
    Ryax API for all services

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: 22.7.0-beta2
    Generated by: https://openapi-generator.tech
"""


import re  # noqa: F401
import sys  # noqa: F401

from ryax_sdk.api_client import ApiClient, Endpoint as _Endpoint
from ryax_sdk.model_utils import (  # noqa: F401
    check_allowed_values,
    check_validations,
    date,
    datetime,
    file_type,
    none_type,
    validate_and_convert_types
)
from ryax_sdk.model.studio_error import StudioError


class WorkflowModulesStaticFileInputApi(object):
    """NOTE: This class is auto generated by OpenAPI Generator
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """

    def __init__(self, api_client=None):
        if api_client is None:
            api_client = ApiClient()
        self.api_client = api_client
        self.studio_workflows_workflow_id_modules_module_id_inputs_workflow_input_id_file_post_endpoint = _Endpoint(
            settings={
                'response_type': None,
                'auth': [
                    'bearer'
                ],
                'endpoint_path': '/studio/workflows/{workflow_id}/modules/{module_id}/inputs/{workflow_input_id}/file',
                'operation_id': 'studio_workflows_workflow_id_modules_module_id_inputs_workflow_input_id_file_post',
                'http_method': 'POST',
                'servers': None,
            },
            params_map={
                'all': [
                    'workflow_id',
                    'module_id',
                    'workflow_input_id',
                ],
                'required': [
                    'workflow_id',
                    'module_id',
                    'workflow_input_id',
                ],
                'nullable': [
                ],
                'enum': [
                ],
                'validation': [
                ]
            },
            root_map={
                'validations': {
                },
                'allowed_values': {
                },
                'openapi_types': {
                    'workflow_id':
                        (str,),
                    'module_id':
                        (str,),
                    'workflow_input_id':
                        (str,),
                },
                'attribute_map': {
                    'workflow_id': 'workflow_id',
                    'module_id': 'module_id',
                    'workflow_input_id': 'workflow_input_id',
                },
                'location_map': {
                    'workflow_id': 'path',
                    'module_id': 'path',
                    'workflow_input_id': 'path',
                },
                'collection_format_map': {
                }
            },
            headers_map={
                'accept': [
                    'application/json'
                ],
                'content_type': [],
            },
            api_client=api_client
        )

    def studio_workflows_workflow_id_modules_module_id_inputs_workflow_input_id_file_post(
        self,
        workflow_id,
        module_id,
        workflow_input_id,
        **kwargs
    ):
        """Update workflow module input with static file  # noqa: E501

        Update the value of module input in a workflow wihth a static file  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.studio_workflows_workflow_id_modules_module_id_inputs_workflow_input_id_file_post(workflow_id, module_id, workflow_input_id, async_req=True)
        >>> result = thread.get()

        Args:
            workflow_id (str):
            module_id (str):
            workflow_input_id (str):

        Keyword Args:
            _return_http_data_only (bool): response data without head status
                code and headers. Default is True.
            _preload_content (bool): if False, the urllib3.HTTPResponse object
                will be returned without reading/decoding response data.
                Default is True.
            _request_timeout (int/float/tuple): timeout setting for this request. If
                one number provided, it will be total request timeout. It can also
                be a pair (tuple) of (connection, read) timeouts.
                Default is None.
            _check_input_type (bool): specifies if type checking
                should be done one the data sent to the server.
                Default is True.
            _check_return_type (bool): specifies if type checking
                should be done one the data received from the server.
                Default is True.
            _spec_property_naming (bool): True if the variable names in the input data
                are serialized names, as specified in the OpenAPI document.
                False if the variable names in the input data
                are pythonic names, e.g. snake case (default)
            _content_type (str/None): force body content-type.
                Default is None and content-type will be predicted by allowed
                content-types and body.
            _host_index (int/None): specifies the index of the server
                that we want to use.
                Default is read from the configuration.
            _request_auths (list): set to override the auth_settings for an a single
                request; this effectively ignores the authentication
                in the spec for a single request.
                Default is None
            async_req (bool): execute request asynchronously

        Returns:
            None
                If the method is called asynchronously, returns the request
                thread.
        """
        kwargs['async_req'] = kwargs.get(
            'async_req', False
        )
        kwargs['_return_http_data_only'] = kwargs.get(
            '_return_http_data_only', True
        )
        kwargs['_preload_content'] = kwargs.get(
            '_preload_content', True
        )
        kwargs['_request_timeout'] = kwargs.get(
            '_request_timeout', None
        )
        kwargs['_check_input_type'] = kwargs.get(
            '_check_input_type', True
        )
        kwargs['_check_return_type'] = kwargs.get(
            '_check_return_type', True
        )
        kwargs['_spec_property_naming'] = kwargs.get(
            '_spec_property_naming', False
        )
        kwargs['_content_type'] = kwargs.get(
            '_content_type')
        kwargs['_host_index'] = kwargs.get('_host_index')
        kwargs['_request_auths'] = kwargs.get('_request_auths', None)
        kwargs['workflow_id'] = \
            workflow_id
        kwargs['module_id'] = \
            module_id
        kwargs['workflow_input_id'] = \
            workflow_input_id
        return self.studio_workflows_workflow_id_modules_module_id_inputs_workflow_input_id_file_post_endpoint.call_with_http_info(**kwargs)

